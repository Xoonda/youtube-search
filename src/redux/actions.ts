import { IResData } from '../../commonInterfaces';

interface INewPayload {
    videos: IResData[],
    title: string,
    vision: boolean
}
export const setVideoList = (payload: INewPayload) => ({ type: 'SET_VIDEO_LIST', payload })


interface IVision {
    vision: boolean;
}
export const setVision = (payload: IVision) => ({ type: 'SET_VISION', payload})

interface IUsername {
    username: string;
}
export const setUsername = (payload: IUsername) => ({ type: 'SET_USERNAME', payload})
