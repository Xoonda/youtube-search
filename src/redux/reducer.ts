import { RootState } from "../../commonInterfaces";

const initialState: RootState = {
    videoList: [],
    searchText: '',
    vision: false,
  }


  
const reducer = (state: RootState = initialState, action: any) => {
    switch (action.type) {
      case 'SET_VIDEO_LIST':
        return {...state, videoList: action.payload.videos, searchText: action.payload.title}
      case 'SET_VISION':
        return {...state, vision: action.payload.vision}
      default:
        return state;
    }
  }

  export default reducer;


