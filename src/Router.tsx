import * as React from 'react';
import { Route, useHistory } from 'react-router-dom';

import Auth from './pages/Auth/Auth';
import Search from './pages/Search/Search';
import Favorites from './pages/Favorites/Favorites';

interface IRouterProps {
}

const Router: React.FunctionComponent<IRouterProps> = (props) => {

  const token: string|null = localStorage.getItem('token');
  const history = useHistory();
  if (token) {
    history.push('/')
  }

  return (
    <>
      <Route path='/' exact component={Search} />
      <Route path='/auth' exact component={Auth} />
      <Route path='/favorites' exact component={Favorites} />
    </>
    
  );
};

export default Router;
