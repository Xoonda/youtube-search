import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Input, Button } from 'antd';
import axios from 'axios';
import { setVideoList} from '../../redux/actions';
import styles from './InputSearchWithoutIcon.module.scss';

interface IProps {
  text: string
}

const pathForSearch = 'http://localhost:4000/api/search/';

const InputSearchWithoutIcon: React.FunctionComponent<IProps> = ({ text }) => {

  const dispatch = useDispatch();
  const [title, setTitle] = useState<string>(text);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): any => {
    setTitle(event.target.value)
  }

  // отправить на бэк запрос видео-лист
  const handleClick = async (): Promise<void> => {
    
    const config = {
      params: {
        q: title
      }

    }

    // принять массив объектов с информацией о видео
    try {
      const serachResult = await axios.get(pathForSearch, config)
    
      dispatch(setVideoList({videos: serachResult.data, title, vision: false}))
      // положим в стейт, будем рендерить из стейта компонент с видео
    } catch (error) {
      console.error(error)
    }
  }


  return (
    <div className={styles['page-req']}>
      <Input placeholder="Что хотите посмотреть?" value={title} onChange={handleChange} />
      <Button onClick={handleClick}>Найти</Button>
    </div>
    // прикрутить редакс(контекст) для сохранения и получения результатов IResData в стор
    // на уровне с server создать файл commonInterfaces, перенести интерфейс IResData и импортировать его на бэк и фронт(export)
    // сбор истории просмотра путём добавления информации(ResData) о видео в JSON
    // отрендерить компоненты, используя полученные данные
    // сохраним в "избранное"
    // ЗАПРОС ТОЛЬКО ВИДЕО (НАПИСАТЬ)
    
  );
};

export default InputSearchWithoutIcon;