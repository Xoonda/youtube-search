import React, { useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { setVision } from '../../redux/actions'
import { IFavReq, RootState } from '../../../commonInterfaces';

import clsx from 'clsx';
import style from './SearchModal.module.scss';
import axios from 'axios';

interface ISearchModuleProps {
  
}


const SearchModule: React.FunctionComponent<ISearchModuleProps> = () => {
  
  const vis = useSelector((state: RootState) => state.vision)
  const title = useSelector( (state: RootState) => state.searchText)
  const videoList = useSelector( (state: RootState) => state.videoList)

  const [requestName, setRequestName] = useState('');

  const pathFromReq = 'http://localhost:4000/api/favorites/';

  // диспатчим событие VISION для отображения блока модального окна
  const dispatch = useDispatch();

  const handleClickToExit = () => {
    dispatch(setVision({ vision: false }))    
  }


  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRequestName(event.target.value)
  } 

  // создание кнопки сохранение запрсоа
  const handleClickToSave = () => {
    // для 1 запроса => мы хотим получить объект с полем: 
          //  содержимое строки запроса,
    //        название запроса, 
    //        массив объектов с параметрами поиска видео
    
    const reqForSave: IFavReq = {
      reqName: requestName,
      inputName: title,
      videoList: videoList
    }
    
    const token = localStorage.getItem('token')
    axios.post<null>(pathFromReq, reqForSave, {headers: {Authorization : "Bearer " + token} })
      .then( res => console.log(res.data))
      .catch ( error => console.log(error))
    
    dispatch(setVision( {vision: false}))
    setRequestName('');
  }
  
  
  return (
    <div className={clsx(style['modal-page-wrapper'],!vis && style['none'] )}>
      <div className={style['modal-page']}>
        <p>Сохранить запрос</p>
        
        <label htmlFor="reqname">Запрос</label>
        <input type="text" placeholder={title} name="-reqname" />

        <label htmlFor="searchname">*Название</label>
        <input type="text" placeholder="Укажите название" name="searchname" value={requestName} onChange={handleChange}/>

        <label htmlFor="sort">Сортировать по</label>
        <input type="text" placeholder="Без сортировки" name="sort" />

        <div className={style['modal']}>

          <button onClick={handleClickToExit}>Не сохранять</button>
          <button onClick={handleClickToSave}>Сохранить</button>

        </div>
      </div>
    </div>
  );
};

export default SearchModule;
