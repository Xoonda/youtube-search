import React from 'react';
import styles from './VideoRender.module.scss'

interface IVideoRenderProps {
  id: string,
  views: number,
  nameChannel: string,
  nameVideo: string
}

const VideoRender: React.FunctionComponent<IVideoRenderProps> = ({ id, views, nameChannel, nameVideo }) => {
  return (
    <>
      <div className={styles['video-card']}>
        <iframe
          width="300"
          height="250"
          src={`https://www.youtube.com/embed/${id}`}
          frameBorder="0"
        >
        </iframe>
        <span className={styles['video-span']}>{nameVideo}</span>
        <span className={styles['channel-span']}>{nameChannel}</span>
        <span className={styles['views-span']}>Views: {views}</span>
      </div>
    </>
  );
};

export default VideoRender;
