import * as React from 'react';
import {Button} from 'antd';
import logo from '../../img/logo.svg';
import styles from './Header.module.scss';
import {NavLink, useHistory} from 'react-router-dom';

interface IMainPageProps {
}


const MainPage: React.FunctionComponent<IMainPageProps> = (props) => {
    const history = useHistory();

    const handleClick = () => {
        const token = localStorage.removeItem('token');
        history.push('/auth')
    }

    const gotoSearch = () => {
        history.push('/')
    }

    const gotoFavorites = () => {
        history.push('/favorites')
    }

    return (
        <div className={styles['header-content']}>
            <div className={styles['header-content-left']}>
                <img className={styles['img-width']} src={logo} alt=""/>
                <Button onClick={gotoSearch}>Поиск</Button>
                <Button onClick={gotoFavorites}>Избранное</Button>

            </div>
            <div className={styles['header-content-right']}>
                <Button onClick={handleClick}>Выйти</Button>
            </div>


        </div>
    );
};

export default MainPage;
