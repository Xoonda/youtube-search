import React, { useState } from 'react';
import { HeartOutlined, UnorderedListOutlined, TableOutlined } from '@ant-design/icons';
import { Input, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { setVideoList, setVision } from '../../redux/actions'
import { RootState } from '../../../commonInterfaces';
import axios from 'axios';
import VideoRender from '../VideoRender/VideoRender';
import styles from './InputSearchWithIcon.module.scss';
import clsx from 'clsx';

interface IProps {
  text: string
}

const pathForSearch = 'http://localhost:4000/api/search/';



const InputSearchWithIcon: React.FunctionComponent<IProps> = ({ text }) => {

  const dispatch = useDispatch();
  const [title, setTitle] = useState<string>(text);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): any => {
    setTitle(event.target.value)
  }

  // отправить на бэк запрос видео-лист
  const handleClick = async (): Promise<void> => {
    const config = {
      params: {
        q: title
      }
    }

    // принять массив объектов с информацией о видео
    try {
      const serachResult = await axios.get(pathForSearch, config)

      dispatch(setVideoList({ videos: serachResult.data, title, vision: false}))
      // положим в стейт, будем рендерить из стейта компонент с видео
    } catch (error) {
      console.error(error)
    }
  }
 
  const handleClickFavoritesQuerry = async () => {
    dispatch(setVision({ vision: true }));
  }

  
  const [counter, setCounter] = useState(false);
  const changeCounterToColumn = () => {
    if(counter == false) {
      setCounter(!counter);
    }
  }
  const changeCounterToRow = () => {
    if(counter == true) {
      setCounter(!counter);
    }
  }


  const videoList = useSelector((state: RootState) => state.videoList)
  
  return (
    <div >
      
      <div className={styles['search-ib']}>
        <Input placeholder="Что хотите посмотреть?" value={title} onChange={handleChange} suffix={<HeartOutlined onClick={handleClickFavoritesQuerry} />} />
        <Button onClick={handleClick}>Найти</Button>
      </div>
      <div className={styles['top-row']}>
        <p>Видео по запросу: {text}</p>
        <div>
          <UnorderedListOutlined className={styles['left-icon']} onClick={changeCounterToColumn}/>
          <TableOutlined className={styles['icon']} onClick={changeCounterToRow}/>
        </div>
      </div>
      <div className={clsx(styles['videolist'], counter && styles['none'])}>

        {videoList.map(item => <VideoRender
          id={item.id}
          views={item.views}
          nameChannel={item.nameChanel}
          key={item.id}
          nameVideo={item.title}
        />)}
      </div>
    </div>
  );
};

export default InputSearchWithIcon;