import * as React from 'react';
import styles from './Auth.module.scss';
import { Form, Input, Button } from 'antd';
import logo from '../../img/logo.svg';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setUsername } from '../../redux/actions';

interface IAuthProps {

}


const Auth: React.FunctionComponent<IAuthProps> = (props) => {

  const pathApi = 'http://localhost:4000/api/users/auth';
  const history = useHistory();


  const onFinish = (values: any) => {
    // отправляем логин и пароль на бэк   

    axios.post(pathApi, values)
      .then(res => {
        // получили токен
        // сохранили в ЛС
        const token = res.data
        localStorage.setItem('token', token);
        // переадресация на main page
      })
      
      // Ошибка
      .catch(err => console.error(err))
    setTimeout( () => {
      history.push('/')
    }, 300)

  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };


  return (
    <div className={styles['auth-form']}>
      <img src={logo} alt="" />
      <Form
        className={styles['formWidth']}
        layout="vertical"
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item>
          <Button className={styles['center']} type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Auth;
