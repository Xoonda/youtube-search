import React, {useEffect, useState} from 'react'

import Header from '../../components/Header/Header';

import style from './Favorites.module.scss';
import axios from "axios";

interface IList {
    reqName: string
    inputName: string
    videoList: []
    username: string
}

const Favorites = () => {
    /*
    * нам нужен массив с видеолистом => как будем рендерить массив? аксиос запрос на бэк
    * для получения массива со списком сохраненных запросов, затем вытащим имя текущего пользователя,
    * пройдёмся по массиву, найдём запросы, подходящие по имени пользователя, сздадим новый массив из них
    * (проще обработать всё на беке) отправляем запрос и токен => получаем массив для конкретного пользователя
    * мапим с тайтлом запроса
    *
    *
    *
    * рендерить ссылки? в любом случае рендер только НАЗВАНИЯ запроса(название инпута или сохрвненного запроса?)
    * функция рендера видео? => нажать на название, открытие страницы с параметрами нужного запроса
     */

    const pathForReq = 'http://localhost:4000/api/favren/';
    const token = localStorage.getItem('token');
    const [arrForRen, setArrForRen] = useState<IList[]>([]);
    const getData = <T,>(a:T,b:T) => {
        if (typeof a === "number" && typeof b === "number") {
            console.log(a + b)
        }
    }
    useEffect( () => {
        axios.post(pathForReq, '',{headers: {Authorization: "Bearer " + token}})
            .then(res => {
                // setArrForRen(res.data);
                setArrForRen( () => res.data)
                console.log(arrForRen);
            })
            .catch(error => console.log(error))
    }, [])

    return (
        <>
            <Header/>
            <div className={style['favorites-wrapper']}>
                <h1>Избранное</h1>
                <div className={style['favorites-list']}>
                    {arrForRen.map( item =>

                        <div>
                            {item.inputName}
                        </div>
                    )}
                </div>
            </div>
        </>
    )
}
export default Favorites;