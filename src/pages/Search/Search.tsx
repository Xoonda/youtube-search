import React, { useState } from 'react';
import InputSearchWithIcon from '../../components/InputSearchWithIcon/InputSearchWithIcon';
import InputSearchWithoutIcon from '../../components/InputSearchWithoutIcon/InputSearchWithoutIcon';
import Header from '../../components/Header/Header'
import { useSelector } from 'react-redux';
import { RootState } from '../../../commonInterfaces';
import styles from './Search.module.scss';
import { useHistory } from 'react-router';
import SearchModal from '../../components/SearchModal/SearchModal';



const Search: React.FunctionComponent = () => {
  const history = useHistory();
  const token = localStorage.getItem('token');
  if (!token) {
    history.push('/auth')
  }

  const videoList = useSelector((state: RootState) => state.videoList);
  const searchText = useSelector((state: RootState) => state.searchText);
  return (
    <>
      <Header />
      <div className={styles['search-page']}>
        {(videoList.length) ? <InputSearchWithIcon text={searchText}/> : <InputSearchWithoutIcon text={searchText}/>}
      </div>
      <SearchModal />
    </>

  );
};

export default Search;
