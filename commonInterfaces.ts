export interface IResData {
  title: string,
  id: string,
  nameChanel: string,
  views: number,
  thumb: string
}

export interface RootState {
  videoList: IResData[],
  searchText: string;
  vision: boolean,
}

export interface IFavReq {
  reqName: string
  inputName: string
  videoList: IResData[]
  username?: string
}