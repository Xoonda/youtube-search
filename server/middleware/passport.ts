import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { PassportStatic } from 'passport';

var opts = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET || '',
}

const passportfunc = (passport: PassportStatic) => {
    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
        return done(null, jwt_payload.username);
    }));
}
export default passportfunc;