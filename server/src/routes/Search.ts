import axios from "axios";
import { Router, Response } from "express";
import { StatusCodes } from "http-status-codes";

interface Req {
    query: {
        q: string
    }
}

interface IResData {
    title: string,
    id: string,
    nameChanel: string,
    views: number,
    thumb: string
}

interface ISnippet {
    id: { videoId: string },
    snippet: {
        title: string,
        channelTitle: string
        thumbnails: {
            medium: {
                url: string
            }
        }
    }
}

const router = Router();
const pathForSearch = 'https://youtube.googleapis.com/youtube/v3/search';
const pathForStatistics = `https://youtube.googleapis.com/youtube/v3/videos`;

const params = {
    key: 'AIzaSyBiMwnBcymKw462kMxVCHnXmWCRb9OUHsQ',
    part: 'snippet',
    maxResults: '12',
    q: ''
}
 

router.get('/', async (req: Req, res: Response<IResData[]>) => {
    // отправить запрос на Y
    try {
        // массив видео по запросу
        const configForSearch = { params: { ...params, q: req.query.q } };
        const snippets = (await axios.get(pathForSearch, configForSearch)).data.items
            .filter((item: ISnippet) => !!item.id.videoId)

        //переформировать массив, чтобы получить ID всех видосиков  
        const ids = snippets.map((item: ISnippet) => item.id.videoId)
            .join(',');

        
        const configForStatistics = { params: { key: params.key, part: 'statistics', id: ids } }
        const statistics = await axios.get(pathForStatistics, configForStatistics)
        

        const resData: IResData[] = snippets.map((item: ISnippet, index: number) => ({
            title: item.snippet.title,
            id: item.id.videoId,
            nameChanel: item.snippet.channelTitle,
            views: statistics.data.items[index].statistics.viewCount,
            thumb: item.snippet.thumbnails.medium.url
        }))

        return res.status(StatusCodes.OK).json(resData);
    } catch (e) {
        console.log(e)
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).end('ts');

    }

})

export default router;