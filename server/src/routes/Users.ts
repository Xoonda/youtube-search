import StatusCodes, { NOT_ACCEPTABLE } from 'http-status-codes';
import { Request, Response, Router } from 'express';

import UserDao from '@daos/User/UserDao.mock';
import { paramMissingError, IRequest } from '@shared/constants';
import jwt from 'jsonwebtoken';

const router = Router();
const userDao = new UserDao();
const { BAD_REQUEST, CREATED, OK } = StatusCodes;



interface User {
    username: string,
    password: string 
}
interface Req {
    body: User
}


/******************************************************************************
 *                      Get All Users - "GET /api/users/all"
 ******************************************************************************/

router.get('/all', async (req: Request, res: Response) => {
    const users = await userDao.getAll();
    return res.status(OK).json({users});
});



/******************************************************************************
 *                       Add One - "POST /api/users/add"
 ******************************************************************************/

router.post('/add', async (req: IRequest, res: Response) => {
    const { user } = req.body;
    if (!user) {
        return res.status(BAD_REQUEST).json({
            error: paramMissingError,
        });
    }
    await userDao.add(user);
    return res.status(CREATED).end();
});



/******************************************************************************
 *                       Update - "PUT /api/users/update"
 ******************************************************************************/

router.put('/update', async (req: IRequest, res: Response) => {
    const { user } = req.body;
    if (!user) {
        return res.status(BAD_REQUEST).json({
            error: paramMissingError,
        });
    }
    user.id = Number(user.id);
    await userDao.update(user);
    return res.status(OK).end();
});



/******************************************************************************
 *                    Delete - "DELETE /api/users/delete/:id"
 ******************************************************************************/

router.delete('/delete/:id', async (req: IRequest, res: Response) => {
    const { id } = req.params;
    await userDao.delete(Number(id));
    return res.status(OK).end();
});

/******************************************************************************
 *                       Auth - "POST /api/users/auth"
 ******************************************************************************/

router.post('/auth', async (req: Req, res: Response) => {
    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(BAD_REQUEST).json({
            error: paramMissingError,
        });
    }
    // шлём запрос в json с введенными данными с формы
    const candidate = await userDao.getOne(username)
    // console.log(candidate)
    // сравниваем данные с формы с записью в дб
    
    
    if (candidate?.login === username && candidate?.password === password) {
        // если ок, то формируем токен пользователя
        const token = jwt.sign({username}, process.env.JWT_SECRET || '', {expiresIn: 60 * 60})
        // отправляем токен на фронт
        return res.status(OK).end(token);
    } else {
        // err
        res.status(StatusCodes.NOT_ACCEPTABLE).json({
            message: 'Неправильный логин и/или пароль'
        })
    }
    
    
});

/******************************************************************************
 *                                     Export
 ******************************************************************************/

export default router;
