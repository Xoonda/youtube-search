import { Router } from 'express';
import fs from 'fs';
import passport from 'passport'

const videoList = require('../../video.json');
const router = Router();

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {

    const newVideoReq = req.body;
    newVideoReq.username = req.user;
    videoList.push(newVideoReq)
    await fs.writeFile('video.json', JSON.stringify(videoList), err => console.log(err))
    res.status(200)
} )


export default router;