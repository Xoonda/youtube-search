import { Router } from 'express';
import UserRouter from './Users';
import SearchRouter from './Search';
import FavRouter from './Favorites';
import FavRenRouter from './FavoritesRender';


// Init router and path
const router = Router();

// Add sub-routes
router.use('/users', UserRouter);
router.use('/search', SearchRouter);
router.use('/favorites', FavRouter);
router.use('/favren', FavRenRouter)



// Export the base-router
export default router;
