import { Router } from 'express';
import passport from 'passport'

const videoList = require('../../video.json');
const router = Router();

router.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {

    const username = req.user;

    const arrOnlyOneUser = videoList.map ( (item: any) => {
        if (item.username === username) {return item}
    })

    const newArrOnlyOneUser = arrOnlyOneUser.filter ( (i:any) => i != undefined)
    res.status(200).send(newArrOnlyOneUser);
//    http://localhost:4000/api/favren/
} )

export default router;