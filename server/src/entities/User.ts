export interface IUser {
    id: number;
    login: string;
    password: string;
}

class User implements IUser {

    public id: number;
    public password: string;
    public login: string;

    constructor(nameOrUser: string | IUser, login?: string, id?: number) {
        if (typeof nameOrUser === 'string') {
            this.password = nameOrUser;
            this.login = login || '';
            this.id = id || -1;
        } else {
            this.password = nameOrUser.password;
            this.login = nameOrUser.login;
            this.id = nameOrUser.id;
        }
    }
}

export default User;
